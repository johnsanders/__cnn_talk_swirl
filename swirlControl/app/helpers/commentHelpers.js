import xml2js from "xml2js";
import axios from "axios";

const commentsUrl = "/apps/misc/cnn_talk/data/approved_comments.json";
const fetchFeed = () => {
	return axios.get(commentsUrl)
		.then( (response) => {
		return response.data.map((comment) => {
			return {
				id: comment.facebookId,
				author: decodeURIComponent(comment.user),
				comment: decodeURIComponent(comment.comment),
				favorite: comment.favorite,
				onScreen: false
			};
		});
	}).catch(function(err) {
		console.error(err);
	});
};

export default fetchFeed;
