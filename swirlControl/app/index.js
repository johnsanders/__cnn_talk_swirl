import React from "react";
import ReactDOM from "react-dom";
import Controller from "./components/MaxControl";
// eslint-disable-next-line no-unused-vars
import Bootstrap from "bootstrap/dist/css/bootstrap.css";
import "./styles/style.css";

ReactDOM.render(<Controller />, document.getElementById("maxControl"));
