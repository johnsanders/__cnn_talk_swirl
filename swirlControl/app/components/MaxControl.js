import React from "react";
import SwirlButtons from "./SwirlButtons";
import Comments from "./Comments";

class MaxControl extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
			<div className="container-fluid" style={{"marginTop":"10px"}}>
				<SwirlButtons />
				<div style={{marginTop:"20px"}}>
					<Comments />
				</div>
			</div>
		);
	}
}

export default MaxControl;
