import React from "react";
import CommentRow from "./CommentRow";
import PropTypes from "prop-types";
class CommentsTable extends React.Component {
	constructor(props) {
		super(props);
		this.state = {activeFeed:0};
		this.getCommentRows = this.getCommentRows.bind(this);
	}
	getCommentRows(fav) {
		const comments = this.props.comments.filter(comment => {
			return fav ? comment.favorite : !comment.favorite;
		});
		const ret = comments.map(comment => (
			<CommentRow
				key={comment.id}
				id={comment.id}
				comment={comment}
				toggleFavorite={this.props.toggleFavorite}
			/>
		));
		if (fav && comments.length > 0) {
			ret.push(<tr key="bebes"><td colSpan="4"></td></tr>);
		}
		return ret;
	}
	render() {
		return this.props.comments.length > 0 ? (
			<div className="row">
				<div className="col-xs-12">
					<table className="table table-striped table-bordered">
						<colgroup>
							<col span="1" style={{width:"5%"}} />
							<col span="1" style={{width:"20%"}} />
							<col span="1" style={{width:"75%"}} />
						</colgroup>
						<tbody>
							{ this.getCommentRows(true) }
							{ this.getCommentRows(false) }
						</tbody>
					</table>
				</div>
			</div>
		) : null;
	}
}
CommentsTable.propTypes = {
	comments:PropTypes.array,
	activeFeed:PropTypes.number,
	toggleFavorite:PropTypes.func
};

export default CommentsTable;
