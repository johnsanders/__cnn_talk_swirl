import React from "react";
import xml2js from "xml2js";
import axios from "axios";
import CommentsTable from "./CommentsTable";
const commentsUrl = "/cake3/cnnitouch/cake-fb-facebookcomments/index.json";
const favoriteUrl = "/cake3/cnnitouch/cake-fb-facebookcomments/setFavorite/1/";

class Comments extends React.Component {
	constructor(props) {
		super(props);
		this.commentFeeds = [];
		this.state = {comments:[], activeFeed:0};
		this.toggleFavorite = this.toggleFavorite.bind(this);
		this.update = this.update.bind(this);
		this.setActiveFeed = this.setActiveFeed.bind(this);
		this.update = this.update.bind(this);
	}
	componentDidMount() {
		this.update();
		this.updateInterval = setInterval(this.update, 5000);
	}
	componentWillUnmount() {
		clearInterval(this.updateInterval);
	}
	toggleFavorite(e) {
		const fbId = e.target.id;
		const newComments = this.state.comments.map(comment => (
			comment.id === fbId ? Object.assign(comment, {favorite:!comment.favorite}) : comment
		));
		const val = newComments.find( c => c.id === fbId ).favorite ? "1" : "0";
		axios.get( favoriteUrl + fbId + "/" + val + ".json" ).then( res => {
			if (res.data.status === "OK") {
				this.setState({comments:newComments});
			}
		});
	}
	setActiveFeed(e) {
		this.setState({activeFeed:parseInt(e.target.name)});
	}
	update() {
		return axios.get(commentsUrl)
			.then( response => {
				const all = JSON.parse(response.data.cakeFbFacebookcomments[0].data);
				const approved = all.filter( comment => {
					const status = comment.moderationStatus;
					return status === "readyForAir" || (status === "moderated" && comment.favorite);
				});
				const comments = approved.map( comment => {
					const currentComment = this.state.comments.find( c => c.id === comment.facebookId );
					if (currentComment) {
						comment.favorite = currentComment.favorite;
					}
					const thing = Object.assign( comment, {
						id: comment.facebookId,
						author: decodeURIComponent(comment.user),
						comment: decodeURIComponent(comment.comment),
					});	
					return thing;
				});
				comments.reverse();
				this.setState({comments});
			}).catch(function(err) {
				console.error(err);
			});
	};
	render() {
		return(
			<CommentsTable
				className="panel-body"
				comments={this.state.comments}
				toggleFavorite={this.toggleFavorite}
				activeFeed={this.state.activeFeed}
			/>
		);
	}
}

export default Comments;
