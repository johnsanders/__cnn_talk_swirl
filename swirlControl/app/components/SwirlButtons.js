import React from "react";
import WebSocket from "reconnecting-websocket";

const dataUrl = "/cake3/cnnitouch/cake-sw-swirlitems/index.json";
class SwirlButtons extends React.Component {
	constructor(props) {
		super(props);
		this.state = {sites:[]} ;
		this.handleClick = this.handleClick.bind(this);
		this.myLoc = "livesites_lon";
	}
	componentDidMount() {
		this.ws = new WebSocket("ws://cnnitouch-prod1:8082");
		this.ws.addEventListener("open", () => {
			// eslint-disable-next-line no-console
			console.log("Multisync socket established");
			this.ws.send(JSON.stringify({command:"register", location:"livesites_lon"}));
		});
		fetch(dataUrl).then(response => {
			return response.json()
		}).then(json => {
			const items = json.cakeSwSwirlitems;
			const haveButtons = items.filter( item => item.hasButton );
			this.setState({sites: haveButtons});
		});
	}
	handleClick(e) {
		if (e.target.classList.contains("showPage")) {
			this.ws.send(JSON.stringify({command:"sendSyncToServer", location:this.myLoc, msg:{command:"show+" + e.target.id}}));
		} else {
			this.ws.send(JSON.stringify({command:"sendSyncToServer", location:this.myLoc, msg:{command:e.target.id}}));
		}
	}
	render() {
		return this.state.sites.length > 0 ? (
			<div>
				<div className="row">
					<div className="col-xs-12">
						<button
							style={{
								margin:"10px",
								padding:"30px"
							}}
							className="btn btn-lg btn-primary"
							id="reset"
							onClick={this.handleClick}
						>
							Reset
						</button>
						{ this.state.sites.map((site) => {
							if (site.hasButton) {
								return (
									<button
										style={{
											margin:"10px",
											padding:"30px"
										}}
										key={site.name}
										id={site.name}
										className="btn btn-lg btn-default showPage"
										onClick={this.handleClick}
									>
										{site.name.replace("_", " ").replace("-", " ").toUpperCase()}
									</button>
								);
							}
						}) }
					</div>
				</div>
			</div>
		) : null;
	}
}

export default SwirlButtons;
