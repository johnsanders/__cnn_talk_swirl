import React from "react";
import PropTypes from "prop-types";

class CommentRow extends React.Component {
	constructor(props) {
		super(props);
		this.unfavClass = "glyphicon glyphicon-star-empty";
		this.favClass = "glyphicon glyphicon-star";
	}
	render() {
		return(
			<tr className={this.props.comment.favorite ? "favorite" : ""}>
				<td className="text-center">
					<button
						id={this.props.id}
						className={"btn btn-default " + (this.props.comment.favorite ? "active" : "")}
						onClick={this.props.toggleFavorite}
					>
						<span
							className={(this.props.comment.favorite ? this.favClass : this.unfavClass)}
							style={{pointerEvents:"none"}}>
						</span>
					</button>
				</td>

				<td style={{verticalAlign:"middle"}}>
					{this.props.comment.author}
				</td>
				<td style={{verticalAlign:"middle"}}>
					{this.props.comment.comment}
				</td>
			</tr>
		);
	}
}
CommentRow.propTypes = {
	comment: PropTypes.shape({
		author:PropTypes.string,
		comment:PropTypes.string,
		favorite:PropTypes.bool,
		onScreen:PropTypes.bool
	}),
	id:PropTypes.string,
	toggleFavorite:PropTypes.func
};

export default CommentRow;
